import argparse
import errno
import json
import logging
import os
import shutil
import sys
from os import path
import re

def parse_arguments():
    parser = argparse.ArgumentParser(description='Standardize data types in pubmed json schema')
    parser.add_argument("source_path", help="Please specify path to pubmed input files to process", type=str)
    parser.add_argument("output_path", help="Please specify output path to write standardized pubmed files", type=str)
    return parser.parse_args()


def get_logger(filename, log_file=None):
    logger = logging.getLogger(filename)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - [%(name)s:%(lineno)d] - %(levelname)s - %(message)s')

    channel = logging.FileHandler(log_file) if log_file else \
        logging.StreamHandler(sys.stdout)
    channel.setLevel(logging.INFO)
    channel.setFormatter(formatter)

    logger.addHandler(channel)

    return logger


logger = get_logger(__file__)
#args = parse_arguments()
source_path = "C:/Users/Mayur Sonawane/Documents/who_deltacon/jsons/" #path.join(args.source_path, '')
output_path = "C:/Users/Mayur Sonawane/Documents/who_deltacon/imp/"#path.join(args.output_path, '')


def write_to_file(content, target_dir, file_name):
    try:
        os.makedirs(target_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    target_file = path.join(target_dir, file_name)
    with open(target_file, 'w') as outfile:
        json.dump(content, outfile)


shutil.rmtree(output_path, ignore_errors=True)

for file in os.listdir(source_path):
    source_file = source_path + file
    if os.path.isfile(source_file) and not file.startswith('.'):
        data = ''
        try:
            with open(source_file, 'r', encoding='utf8') as input_file:
                data = json.loads(input_file.read())
                newData = {}
                imp_keys = ["who_id","entry_date","ti_en","ti","ta","au","afiliacao_autor","ab","entry_date"]
                single_str = ["ti_en","ti","ta"]
                list_str = ["au","afiliacao_autor","ab"]
                for key in imp_keys:
                    newData[key] = "" if key not in list_str else []

                newData["id"] = data["id"] if data['id'] else ""
                newData["who_id"] = re.findall('\d+',data["id"])[0]   ###id

                str_arr= data["doc"]["str"]
                for i, d in enumerate(str_arr):
                    if d["name"] == "entry_date":
                        newData["entry_date"] = d["text"]
                        break



                arr = data["doc"]["arr"]
                for i, d in enumerate(arr):
                    if d["name"] in imp_keys:
                        if isinstance(d["str"], list):
                            newData[d["name"]] = d["str"] if d["name"] not in single_str else d["str"][0]
                        else:
                            newData[d["name"]] = [d["str"]] if d["name"] not in single_str else d["str"]

                write_to_file(newData, output_path, file)

        except Exception as e:
            logger.info("Unable to process file: {} : {}".format(file, e, e.with_traceback()))



